package hu.nedlab.commands;

/**
 * Created by attila on 06/05/16.
 */
public class GetRealTimeClock extends BaseCommand {
    byte node;
    private static final byte commandcode = 0x24;

    public GetRealTimeClock(byte node) {
        // length from the documentation
        super((byte)0x04);
        this.node = node;

        buildCommand();

        checksum();
    }

    @Override
    protected void buildCommand() {
        if (command != null){
            command[2] = node;
            command[3] = commandcode;
        }
    }

}

package hu.nedlab.helper;

/**
 * Created by attila on 06/05/16.
 *
 * End of the commands need XOR and SUM checksum operation
 *
 */
public class CheckSum {
    public static byte xor(byte[] param){
        byte result = (byte) 0xFF;
        if (param != null && param.length > 0){
            for (byte h : param) {
                result ^= h;
            }
        }
        return result;
    }

    public static byte sum(byte[] param){
        byte result = 0;
        if (param != null && param.length > 0){
            for (byte h : param) {
                result += h;
            }
        }
        result %= 256;
        return result;
    }
}
